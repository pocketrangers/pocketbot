/* ----------------------------------------
	This file controls all the events fired
	off by presence changes from users, i.e.
	status, "Now playing...", and streams
 ---------------------------------------- */
var exports = module.exports = {};

let logger  = require("./logger"),
	// dio		= require("./dio"),
	x 		= require("./vars"),
	helper	= require("./helpers");

exports.onChange = async function(status, userdata=null) {
	const fromID = status.userID,
		game = status.game,
		timer = new Date().getTime(),
		mem = status.e.d.member,
		fromRoles = status.e.d.roles || null,
		more = status.e.d.activities;

	// Someone goes offline
	if (status.state === "offline") {
		// Check if they are on ready list
		if (fromRoles && fromRoles.includes(x.lfg)) {
			status.bot.removeFromRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			}, function(err, resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}
			});
		}

		// Copy user to FB
		userdata.setProp({ user: mem, prop: { name: "state" } });

		// // Save timestamp for dev
		if (fromRoles && fromRoles.includes(x.admin) ) userdata.setProp({ 
			user: fromID, 
			prop: { 
				name: "onlast", 
				data: timer 
			}
		});
	}

	// Someone is playing/streaming (?) the game
	if (game) {
		let gameName = game.name.toLowerCase();
		// streamer = ( game.hasOwnProperty("url") ) ?  game.url.substr(game.url.lastIndexOf("/") + 1) : null;
		let otherJunk = (more) ? more.map((a) => { return a.name === "Tooth and Tail" ? "tnt" : false; }) : [];

		// Check for all known game names and stream stuff 
		if ( gameName.match(/tooth\s?(and|&)\s?tail/gi) || gameName.includes("tnt") || otherJunk.includes("tnt") ) {
			// Add rank (if playing ranked)
			if (game.state && game.state.startsWith("Ranked (")) {
				// Grab the number, if there is one, which there should...
				const rank = (game.state.match(/([0-9])\w+/gi)) ? game.state.match(/([0-9])\w+/gi)[0] : false;

				if (!rank) return false;
				userdata.setProp({ 
					user: fromID, 
					prop: { 
						name: "rank", 
						data: parseInt(rank) 
					}
				});
			}

			if (!helper.isHeroku() || (fromRoles && fromRoles.includes(x.lfg))) return false;

			// Add to LFG
			status.bot.addToRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			}, (err, resp) => {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}

				// And if the user is not a Recruit OR Veteran
				if (fromRoles && !fromRoles.includes(x.noob) && !fromRoles.includes(x.member)) {
					// Make em recruit
					status.bot.addToRole({
						serverID: x.chan,
						userID: fromID,
						roleID: x.noob
					});
				}
			});
		} else {
			// If he's not playing/streaming it, and has LFG, remove
			if (fromRoles && fromRoles.includes(x.lfg)) {
				status.bot.removeFromRole({
					serverID: x.chan,
					userID: fromID,
					roleID: x.lfg
				});
			}
		}
	} else {
		// Or if he stopped playing/streaming, remove LFG
		if (fromRoles && fromRoles.includes(x.lfg)) {
			status.bot.removeFromRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			});
		}
	}

	// Log all changes in debug mode
	//if ( helper.isDebug() ) logger.log(`User '${from}' is now ${status.state}`);
};
