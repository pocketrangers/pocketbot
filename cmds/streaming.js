/* ----------------------------------------
	This file controls all the stuff related
	to Twitch streams.
 ---------------------------------------- */

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	req 	= require("request"),
	TOKEN 	= require("../core/tokens"),
	dio		= require("../core/dio");

let cmdStream = new command("streaming", "!stream", "Links to the Pocketwatch stream", function(data) {
	dio.say("Check us out on Twitch @ http://www.twitch.tv/Pocketwatch", data);
});

let streamTimer = false;

let cmdTwitch = new command("streaming", "!streams", "Checks Twitch for anyone streaming TnT", (data) => {
	// Check timer to use
	if (streamTimer) {
		dio.say("🕑 I _just_ grabbed the list a moment ago! Try again later or just look at the list here: <https://www.twitch.tv/directory/game/Tooth%20and%20Tail>",data);
		return false;
	}

	dio.del(data.messageID,data);
	dio.say("🕑 Let me see if I can find any...",data);
	req({
		url: "https://api.twitch.tv/helix/streams?game_id=491322",
		headers: { "Client-ID": TOKEN.TWITCHID }
	}, (err, resp, body) => {
		if (err) {
			logger.log(err, logger.MESSAGE_TYPE.Error);
			dio.say("🕑 I had trouble with that request. :sob:",data);
		} else {
			let streams = JSON.parse(body).data;
			if (!streams || streams.length === 0) {
				let v = [
						"Doesn't look like anyone is streaming at the moment.",
						"No active streamers found.",
						"I guess everyone hates this game, since no one is streaming it atm. :sob:",
						"No one is streaming right now, why don't you start one? :D",
						"Your stream princess is in another castle. :confused:"
					],
					n = Math.floor( Math.random()*4 );

				dio.say(`🕑 ${v[n]}`, data);
			} else if (streams.length === 1) {
				//console.log(streams.streams[0].channel);
				let streamer = streams[0].user_name,
					v = [
						`Oh hey, looks like ${streamer} is streaming over at: <https://twitch.tv/${streamer}>`,
						`${streamer} is streaming over right now! Check it out: <https://twitch.tv/${streamer}>`,
						`I found one streamer, ${streamer}, streaming at: <https://twitch.tv/${streamer}>`,
						`Got one! ${streamer} is streaming: <https://twitch.tv/${streamer}>`,
						`Apparently some noob named ${streamer} is streaming atm: <https://twitch.tv/${streamer}>`
					],
					n = Math.floor( Math.random()*4 );

				dio.say(v[n], data);
				streamTimer = true;
				setTimeout(() => { streamTimer = false; }, 1000*2);
			} else if (streams.length > 1) {
				let streamers = [];

				// Max to 6
				for (let i=0,  s=6; i<s; i++) {
					streamers.push(`:movie_camera: **${streams[i].user_name}** | <https://twitch.tv/${streams[i].user_name}>`);
				}

				if (streams.length > 6) {
					streamers.push(`\n...and **${streams.length - 6}** other(s) too! <https://www.twitch.tv/directory/game/Tooth%20and%20Tail>`);
				}

				dio.say(`A few people are streaming! \n${streamers.join(" \n")}`, data);
				streamTimer = true;
				setTimeout(() => { streamTimer = false; }, 1000*2);
			}
		}
	});
});


module.exports.commands = [cmdStream, cmdTwitch];
