/* ----------------------------------------
	This file contains the updated version
	of the !info command from Mastabot.
 ---------------------------------------- */

let command = require("../core/command").Command,
	x 		= require("../core/vars"),
	dio 	= require("../core/dio"),
	helpers	= require("../core/helpers"),
	logger  = require("../core/logger"),
	udata 	= require("../core/unitdata");

function listStuff(l,data,u=false) {
	//console.log("LISTING");
	let list = [];
	Object.keys(l).forEach( (el) => {
		list.push(el);
	});

	if (u) {
		dio.say(`🕑 Available units: \n \`\`\`${list.join(" | ")} \`\`\` ` , data);
	} else {
		dio.say(`🕑 Available traits: \n \`\`\`${list.join(" | ")} \`\`\` ` , data);
	}
}

let cmdListUnits = new command("unitinfo", "!units", "Shows you a list of units", function(data) {
	listStuff(udata.u.units, data, true);
});

let cmdListTraits = new command("unitinfo", "!traits", "Shows you traits of a given unit", function(data) {
	listStuff(udata.u.filters.traits, data);
});

let cmdInfo = new command("unitinfo", "!info", "Shows you information on a given unit", function(data) {
	let item = (data.args[1]) ? data.args[1].toLowerCase() : "",
		u = udata.u;

	// Aliases for unit names
	switch (item) {
	case "squirrels":
		item = "squirrel";
		break;
	case "lizards":
		item = "lizard";
		break;
	case "toads":
		item = "toad";
		break;
	case "moles":
		item = "mole";
		break;
	case "pigeons":
		item = "pigeon";
		break;
	case "ferrets":
		item = "ferret";
		break;
	case "skunks":
		item = "skunk";
		break;
	case "falcons":
		item = "falcon";
		break;
	case "snakes":
		item = "snake";
		break;
	case "wire":
	case "bw":
	case "barbed wire":
	case "barbwire":
		item = "barbedwire";
		break;
	case "mine":
	case "mines":
		item = "landmine";
		break;
	case "cham":
	case "chams":
	case "chameleons":
		item = "chameleon";
		break;
	case "sniper balloon":
	case "sniperballoon":
	case "sniper":
	case "baloon":
		item = "balloon";
		break;
	case "turret":
	case "turrets":
	case "nest":
	case "machine gun":
	case "mg":
	case "mgn":
		item = "machinegun";
		break;
	case "arty":
		item = "artillery";
		break;
	case "mice":
		item = "mouse";
		break;
	case "farm":
		item = "pig";
		break;
	case "windmill":
	case "mill":
		item = "gristmill";
		break;
	}

	// Check if unit exists
	if ( u.units.hasOwnProperty(item) ) {
		let label = (u.units[item].label != undefined) ? u.units[item].label : u.units[item].name;

		// If label still sucks
		if (label === undefined) label = "???";

		// Get some of the basic unit data
		let traits = (u.units[item].traits) ? u.units[item].traits : [],
			cost = (u.units[item].cost != undefined) ? u.units[item].cost : "n/a",
			range = "n/a",
			tier = (u.units[item].tier) ? `[T${Math.ceil(u.units[item].tier)}] ` : "n/a";

		// Traits Checker
		if (traits.length > 0) {
			for (let t in traits) {
				if ( u.filters.traits[ traits[t] ].hasOwnProperty("wpn") ) {
					// Is weapon, get range
					let w = u.filters.traits[ traits[t] ].wpn.replace("weapon_","");
					range = u.weapons[w].AtkRange;
				}
			}

			if (traits.length === 1) {
				let w = u.filters.traits[ traits[0] ];
				traits = `\`\`\`${w.label} - ${w.desc} \`\`\``;
			} else {
				traits = `Traits: \`${traits.join("`, `")}\``;
			}
		}

		// More Info
		let warren = (!u.units[item].struct) ? x.emojis[`warrent${u.units[item].tier}`] : "",
			ucost = (!u.units[item].struct && u.units[item].tier) ? `(${u.units[item].ucost}/unit)` : "",
			url = "",
			btime = (u.units[item].time) ? `:stopwatch: ${u.units[item].time}s`: "";

		// Wiki Link
		if (u.units[item].struct) {
			if (item != "pig" && item != "gristmill") {
				url = `\n\nMore info: <https://toothandtailwiki.com/structures/${item}>`;
			} else {
				url = "\n\nMore info: <https://toothandtailwiki.com/structures/gristmills-farms-pigs/>";
			}
		} else if (!u.units[item].struct && tier != "n/a") {
			let t = u.units[item].tier;
			url = (t && t != 3) ? `\n\nMore info: <https://toothandtailwiki.com/units/${item}s>` : `\n\nMore info: <https://toothandtailwiki.com/units/${item}>`;
		} else {
			url = "\n\nMore info: <https://toothandtailwiki.com/>";
		}
		ucost = `${ucost}   `;
		if(u.units[item].hasOwnProperty("traits") && u.units[item].traits.includes("summon")) {
			warren = "";
			ucost = "    ";
		}
		let atk = (u.units[item].atk) ? `:crossed_swords: **${u.units[item].atk}**    `: "";

		// Create embed
		let embed = new helpers.Embed({
			title: `${label}`,
			color: 0x33ff33,
			description: `aka ${u.units[item].name} | **${(tier != "n/a") ? tier : ""}** ${(warren != undefined) ? warren: ""}

${atk}:shield: **${u.units[item].def}**    :pig2: **${cost}** ${ucost}:gun: **${range}**    ${btime}
${traits} ${url}`,
			thumbnail: {
				url: `https://gitlab.com/pocketrangers/pocketbot/raw/develop/assets/unit_${item}.png`,
				width: 48,
				height: 48
			}
		});

		dio.sendEmbed(embed, data);

		return false;
	} else if (u.filters.traits[item]) {
		let t = u.filters.traits[item],
			w = (t.wpn) ? ":gun:" : "",
			wpn = (t.wpn) ? u.weapons[ t.wpn.replace("weapon_","") ] : "",
			extra = (w != "") ? `\n\n \`Cooldown: ${wpn.cool} | Range: ${wpn.AtkRange} | Aggro: ${wpn.AggRange}\`` : "";

		dio.say(`${w} **${t.label}** - ${t.desc}${extra}`, data);
	} else {
		dio.say("I don't recognize that unit/trait. Try `!units` or `!traits` to get a list.", data);
		return false;
	}
});

let cmdBalAdd = new command("unitinfo", "!baladd", "Adds a unit change to proposed changes.", async (data) => {
	dio.del(data.messageID,data);

	let unitChange = {
		unit: data.args[1].replace("<:tnt","").match(/(\D*)[^:\d>]/g)[0],
		change: data.args.slice(2).join(" ")
	};

	let push = await data.db.balance.child("newchanges").push(unitChange);

	let embed = new helpers.Embed({
		color: 0x85144b,
		description: `**${unitChange.unit}:** ${unitChange.change}`,
		author: {
			icon_url: `https://gitlab.com/pocketrangers/pocketbot/raw/develop/assets/unit_${unitChange.unit}.png`,
			name: "Proposed Balance Change"
		},
		footer: { text: `${push.key}` }
	});

	dio.sendEmbed(embed, data);
});

cmdBalAdd.permissions = [x.mod, x.admin];

let cmdBalDel = new command("unitinfo", "!baldel", "Removes a unit change to proposed changes.", (data) => {
	dio.del(data.messageID,data);

	let u = data.args[1].replace("<:tnt","").match(/(\D*)[^:\d>]/g)[0];
	data.db.balance.child("newchanges").orderByChild("unit").equalTo(u).once("value", (snap) => {
		let val = snap.val();
		if (!Object.keys(val)[0]) return false;

		let key = Object.keys(val)[0],
			msgID = val[key].msgID,
			chanID = val[key].chanID;

		// Delete old message, not using dio cuz I need callback
		data.bot.deleteMessage({
			channelID: chanID,
			messageID: msgID
		}, (err) => {
			if (err) {
				logger.log(err, "Error");
				return false;
			} else {
				// Now delete FB entry
				data.db.balance.child("newchanges").child(key).set({});
			}
		});
	});
});

cmdBalDel.permissions = [x.mod, x.admin];

module.exports.commands = [cmdInfo, cmdListUnits, cmdListTraits, cmdBalAdd, cmdBalDel];
